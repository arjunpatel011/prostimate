//
//  editProfileViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class editProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnBackNavigationSelected(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangePasswordSelected(_ sender: Any) {
        
        let changePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "chnagePasswordViewController") as! chnagePasswordViewController
        
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
}
