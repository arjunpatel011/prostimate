//
//  myAccountViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import SideMenu
class myAccountViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

//MARK: - Button Action methods -

extension myAccountViewController {
   
    @IBAction func btnMenuSelected(_ sender: Any) {
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        SideMenuManager.default.menuShadowRadius = 0
        
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)

    }
    
    
    @IBAction func btnMyPlanSelected(_ sender: Any) {
        let myPlanVC = self.storyboard?.instantiateViewController(withIdentifier: "myPlanViewController") as! myPlanViewController
        
        self.navigationController?.pushViewController(myPlanVC, animated: true)
        
    }
    @IBAction func btnDrinkHistorySelected(_ sender: Any) {
        
        let historyVC = self.storyboard?.instantiateViewController(withIdentifier: "drinkHistoryViewController") as! drinkHistoryViewController
        self.navigationController?.pushViewController(historyVC, animated: true)
    }
    
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "editProfileViewController") as! editProfileViewController
        self.navigationController?.pushViewController(editProfileVC, animated: true)
        
        
    }
    @IBAction func btnPrivacySelected(_ sender: Any) {
        
    }
    
    @IBAction func btnTermsConditionSelected(_ sender: Any) {
        
    }
    @IBAction func btnNotificationSelected(_ sender: Any) {
        
    }
}
