//
//  drinkHistoryViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage
class drinkHistoryViewController: UIViewController {

    @IBOutlet var tblHistory: UITableView!
    
    var historyArray = [shopingHistory]()
    private let services = shopingServices()
    let activityLoader = ActivityData()
    let messagesObj = messagessPopup()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.retriveShopingHistory()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: - Textfiled delegate and datasources methods -

extension drinkHistoryViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
      
        return self.shopingHistory.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblHistory.dequeueReusableCell(withIdentifier: "drinkHistoryTableViewCell") as! drinkHistoryTableViewCell
        

         let drinksArray = self.historyArray[indexPath.section]
            
            if let drink = drinksArray[indexPath.row] as! historyDrink  {
            
            cell.lblItemName.text = drink.drinkName!
            cell.lblItemAddress.text = drinksArray.shopName!
            cell.lblDate.text = drink.created!
            cell.imgItem.sd_setImage(with: URL(string: drink.drinkImage!), placeholderImage: UIImage(named: "img1"))
                cell.sliderImage.addSubview(imgOne)
        }
        
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.historyArray[section].drink?.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 96
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0
    }
    
    
}

//MARK : - Button Action methods -

extension drinkHistoryViewController {
    
    @IBAction func btnBackNaviSelcted(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Webservices function -

extension drinkHistoryViewController {
    
  
    func retriveShopingHistory(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityLoader)
        
        self.services.retriveShopHistory() { (result) in
       
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            switch (result){
                
            case let .Success(shopHistory):
                
                self.historyArray = shopHistory
                self.tblHistory.reloadData()
                
            case .Offline:
                
                print("Offline")
                self.messagesObj.errorMessages(message: internetConnectivity)
                
            case .NotFound:
                
                self.messagesObj.warrningMessages(message: "History not found")
                print("Notfound")
                
            default:
                
                self.messagesObj.warrningMessages(message: "History not found")
                print("defalit")
                break
            }
            
        }
        
    }
}
