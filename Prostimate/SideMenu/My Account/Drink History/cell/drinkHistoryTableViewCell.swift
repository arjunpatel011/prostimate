//
//  drinkHistoryTableViewCell.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class drinkHistoryTableViewCell: UITableViewCell {
   
  
    @IBOutlet var lblTotlItems: UILabel!
    @IBOutlet var imgItem: UIImageView!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblItemAddress: UILabel!
    @IBOutlet var lblItemName: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
