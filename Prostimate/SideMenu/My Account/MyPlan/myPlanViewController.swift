//
//  myPlanViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 11/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class myPlanViewController: UIViewController {

    @IBOutlet var btnBackNavigation: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK : - Button Action methods -

extension myPlanViewController {
    
    @IBAction func btnBackNavSelected(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
