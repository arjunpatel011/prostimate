//
//  progressbarTableViewCell.swift
//  Prostimate
//
//  Created by Arjun Patel on 11/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class progressbarTableViewCell: UITableViewCell {
    @IBOutlet var progressbar: UIProgressView!
    
    @IBOutlet var lblprogress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
