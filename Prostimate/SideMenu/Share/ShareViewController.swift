//
//  ShareViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 11/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import SideMenu
class ShareViewController: UIViewController {

    @IBOutlet var tblShare: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tblShare.estimatedRowHeight = 157;
        self.tblShare.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


    @IBOutlet var btnShareSelected: UIButton!
}

//MARK: - Textfiled delegate and datasources methods -

extension ShareViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
       
        case 0:
             let cell = self.tblShare.dequeueReusableCell(withIdentifier: "cell")
             return cell!
           
        case 1:
            let cell = self.tblShare.dequeueReusableCell(withIdentifier: "cell1")
            return cell!
            
        case 2:
            let cell = self.tblShare.dequeueReusableCell(withIdentifier: "cell2")
            return cell!
            
        case 3:
            let cell = self.tblShare.dequeueReusableCell(withIdentifier: "progressbarTableViewCell") as! progressbarTableViewCell
            
            cell.progressbar.progress = 0.4

            return cell
            
        case 4:
            let cell = self.tblShare.dequeueReusableCell(withIdentifier: "cell4")
            return cell!
        
       
        default:
            
            let cell = self.tblShare.dequeueReusableCell(withIdentifier: "cell4")
            return cell!
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
   
}

//MARK : - Button Selected -

extension ShareViewController {
    
    @IBAction func btnShareSelected(_ sender: Any) {
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        SideMenuManager.default.menuShadowRadius = 0
        
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
}

