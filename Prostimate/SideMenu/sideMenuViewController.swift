//
//  sideMenuViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import SideMenu
class sideMenuViewController: UIViewController {

    @IBOutlet var tblsideMenu: UITableView!
    private var sideMenuArray = [sideMenu]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        self.createSideMenu()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

//MARK: - Custome menu -

extension sideMenuViewController {
   
    /**
     This function is used to init Menu.
 */
    func createSideMenu(){
       
        let iconArray :[String] = ["home","account","referral","faq","contact"]
        let menuArray :[String] = ["Home","Account","Referral","FAQ","Contact Us"]

        for index in 0...iconArray.count - 1 {
            
            let sidemenuObj = sideMenu.init(icon: iconArray[index], title: menuArray[index])
            self.sideMenuArray.append(sidemenuObj)
            
        }
        print("Side menu = \(self.sideMenuArray)")
        
    }
}

//MARK: - Textfiled delegate and datasources methods -

extension sideMenuViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblsideMenu.dequeueReusableCell(withIdentifier: "sideMenuTableViewCell") as! sideMenuTableViewCell
        
        let menuObj = self.sideMenuArray[indexPath.row]
        
        cell.lblMenu.text = menuObj.title
        cell.imgIcon.image = UIImage.init(named: menuObj.icon)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.sideMenuArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        
        case 0:
            
            let mainTabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "mainTabbar")
            self.navigationController?.pushViewController(mainTabbarVC!, animated: true)
            
        case 1:
      
            let myAccountVC = self.storyboard?.instantiateViewController(withIdentifier: "myAccountViewController") as! myAccountViewController

            self.navigationController?.pushViewController(myAccountVC, animated: true)
        
        case 2:
            
            let shareVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
            self.navigationController?.pushViewController(shareVC, animated: true)
            
            
        default:
            
            print("break")
        }
    }
    
}
