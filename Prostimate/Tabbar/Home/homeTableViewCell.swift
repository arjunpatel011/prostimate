//
//  homeTableViewCell.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class homeTableViewCell: UITableViewCell {

    @IBOutlet var lblBarName: UILabel!
    @IBOutlet var lblFar: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var imgBg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
