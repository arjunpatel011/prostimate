//
//  searchViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import SideMenu
import NVActivityIndicatorView
import SDWebImage

class searchViewController: UIViewController {

    @IBOutlet var txtSearch: customTetxtFiled!
    @IBOutlet var tblHome: UITableView!
    
    var shlopingLsitArray = [shopList]()
    private let services = shopingServices()
    
    let activityLoader = ActivityData()
    let messagesObj = messagessPopup()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.retriveShopingList(lat: "23.1008971", long: "72.5260449")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
  

    
}

//MARK: - Button Action methods -

extension searchViewController {
    
    @IBAction func btnMenuSelected(_ sender: Any) {
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
       
        SideMenuManager.default.menuShadowRadius = 0
        
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)

    }
}

//MARK: - Tableview delegate and datasorce methods -

extension searchViewController :UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return shlopingLsitArray.count
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblHome.dequeueReusableCell(withIdentifier: "homeTableViewCell") as! homeTableViewCell
        
        let shopObj = shlopingLsitArray[indexPath.row]
        
        if let neighbour = shopObj.neighbourhood {
            
            cell.lblAddress.text = neighbour
        }
        
        if let far = shopObj.distance {
            
            cell.lblFar.text = far
        }
        
        if let name = shopObj.name {
            
            cell.lblBarName.text = name
        }
        
        
        cell.imgBg.sd_setImage(with: URL(string: shopObj.imageProfile!), placeholderImage: UIImage(named: "img1"))
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let shopObj = self.shlopingLsitArray[indexPath.row]
        
        if let shopId = shopObj.shopId {
            
            self.retriveShopDetail(shopID: shopId,name: shopObj.name!)
        }
    }
    
}

//MARK: - Webservices methods -

extension searchViewController {
    
    func retriveShopDetail(shopID:String,name:String){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityLoader)
        
        self.services.retriveShopDetail(shopId: shopID) { (result) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            
            switch (result){
                
            case let .Success(shopObj):
                
                let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailViewController") as! detailViewController
                detailVC.selectedShopDetail = shopObj
                self.navigationController?.pushViewController(detailVC, animated: true)
                
            case .Offline:
                
                print("Offline")
                self.messagesObj.errorMessages(message: internetConnectivity)
                
            case .NotFound:
                
                self.messagesObj.warrningMessages(message: "\(name) details not found")
                print("Notfound")
                
            default:
                
                self.messagesObj.warrningMessages(message: "\(name) details not found")
                print("defalit")
                break
            }
            
        }
        
    }

    
    func retriveShopingList(lat:String,long:String){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityLoader)
        
        self.services.retriveShopingList(latitude: lat, longitude: long) { (result) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            switch (result){
                
            case let .Success(user):
                
                self.shlopingLsitArray = user
                self.tblHome.reloadData()
                
            case .Offline:
                
                print("Offline")
                self.messagesObj.errorMessages(message: internetConnectivity)
                
            case .NotFound:
                
                self.messagesObj.warrningMessages(message: "Shop list not found")
                print("Notfound")
                
            default:
                
                self.messagesObj.warrningMessages(message: "Shop list not found")
                print("defalit")
                break
            }
            
        }
        
    }
}


