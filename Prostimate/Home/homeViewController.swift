//
//  homeViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 06/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class homeViewController: UIViewController {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var paginationSlider: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.createSilder()
        
        self.lblMessage.text = "A Free Drink Everyday... For The Price!"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: - UI Class -
extension homeViewController :UIScrollViewDelegate{
    
    func createSilder(){
        
        self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.view.frame.height
        
        print("\(scrollViewWidth)")
        
        for index in 0...2 {
           
            let imgOne = UIImageView(frame: CGRect(x:(CGFloat(index) * scrollViewWidth), y:0,width:scrollViewWidth, height:scrollViewHeight))
            
            imgOne.image = UIImage(named: "splash\(index)")
            
            self.scrollView.addSubview(imgOne)
            
        }

        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 3, height:self.view.frame.height)
        self.scrollView.delegate = self
        self.paginationSlider.currentPage = 0
    }
    
    //Scrollview delegate methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Test the offset and calculate the current page after scrolling ends
        
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        // Change the indicator
        
        self.paginationSlider.currentPage = Int(currentPage);
        
        // Change the text accordingly
        
        print("Index = \(Int(currentPage))")
        
        switch Int(currentPage) {
            
        case 0:
            
            self.lblMessage.text = "A free drink everyday... for the price!"
            
        case 1:
            
            self.lblMessage.text =  "Explore cool bars together with your friends."
        default:
            
            self.lblMessage.text = "Discover new drinks and say cheers."
        }
    }
 

}
