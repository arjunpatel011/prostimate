//
//  detailViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import SDWebImage
class detailViewController: UIViewController {

    @IBOutlet var tblDetail: UITableView!
    private let imageSliderDetafultHeight:CGFloat = 200.00
    var suggestedItemsCell:suggestedShopTableViewCell!
    var selectedShopDetail:shopDetail!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        self.tblDetail.estimatedRowHeight = 200
//        self.tblDetail.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}
//MARK: - Button selected methods -
extension detailViewController {
    
    @IBAction func btnCallSelected(_ sender: Any) {
        
        guard let number = URL(string: "tel://\(self.selectedShopDetail.mobile!)") else {
            return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func btnBackNavigationSelected(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: - Tableview delegate and datasorce methods -

extension detailViewController :UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 6
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.row {
        
        case 0:
        
            var cell = self.tblDetail.dequeueReusableCell(withIdentifier: "sliderImageTableViewCell") as! sliderImageTableViewCell
            
            cell = self.createSilder(cell: cell)
            
            return cell
            
        case 1:
            
            let cell = self.tblDetail.dequeueReusableCell(withIdentifier: "shopNameTableViewCell") as! shopNameTableViewCell
            
            if let name = self.selectedShopDetail.name {
                
                cell.lblShopName.text = name
            }
            
            if let address = self.selectedShopDetail.address {
                
                cell.lblAddress.text = address
            }
            return cell
        case 2:
            
            let cell = self.tblDetail.dequeueReusableCell(withIdentifier: "shopDescriptionTableViewCell") as! shopDescriptionTableViewCell
            
            if let description = self.selectedShopDetail.description {
                
                cell.lblDescription.text = description
            }
            
            return cell
        case 3:
            
            let cell = self.tblDetail.dequeueReusableCell(withIdentifier: "shopOpeningTableViewCell") as! shopOpeningTableViewCell
            
            for index in 0...(cell.lblOpening.count-1) {
                
                let label = cell.lblOpening[index]
                
                switch index {
                    
                case 0:
                    
                    if let openTime = self.selectedShopDetail.mondayTime {
                        
                        label.text = openTime
                    }
                case 1:
                    
                    if let openTime = self.selectedShopDetail.tuesdayTime {
                        
                        label.text = openTime
                    }
                case 2:
                    
                    if let openTime = self.selectedShopDetail.wednsdayTime {
                        
                        label.text = openTime
                    }
                case 3:
                    
                    if let openTime = self.selectedShopDetail.thrusdayTime {
                        
                        label.text = openTime
                    }
                case 4:
                    
                    if let openTime = self.selectedShopDetail.fridayTime {
                        
                        label.text = openTime
                    }
                case 5:
                    
                    if let openTime = self.selectedShopDetail.saturdayTime {
                        
                        label.text = openTime
                    }
                case 6:
                    
                    if let openTime = self.selectedShopDetail.sundayTime {
                        
                        label.text = openTime
                    }
                default:
                    
                    print("break")
                    break
                }
                
            }
            
            return cell
        case 4:
            
            let cell = self.tblDetail.dequeueReusableCell(withIdentifier: "cell")
            
            return cell!
        
        case 5:
            
            let cell = self.tblDetail.dequeueReusableCell(withIdentifier: "suggestedShopTableViewCell") as! suggestedShopTableViewCell
            
            self.suggestedItemsCell = cell
            
            cell.collectionSuggestedItems.delegate = self
            
            return cell
        
            
        default:
            
            var cell = self.tblDetail.dequeueReusableCell(withIdentifier: "sliderImageTableViewCell") as! sliderImageTableViewCell
            
            cell = self.createSilder(cell: cell)
            
            return cell
        }
        
        
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return self.imageSliderDetafultHeight
        case 1:
            return UITableViewAutomaticDimension
        case 2:
            return 63
        case 3:
            return 200
        case 4:
            return 44
        case 5:
            return 182
        default:
            
            return 100
        }
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    }
    
}
//MARK: - Collection View delegate and datasource methods -

extension detailViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.selectedShopDetail.suggestedItmsList?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.suggestedItemsCell?.collectionSuggestedItems.dequeueReusableCell(withReuseIdentifier: "suggestedItemCollectionViewCell", for: indexPath) as! suggestedItemCollectionViewCell
        
        
        if  let suggestedItem = self.selectedShopDetail.suggestedItmsList?[indexPath.row] {
        
            cell.imgProduct.sd_setImage(with: URL(string: suggestedItem.imageUrl!), placeholderImage: UIImage(named: "img1"))
            cell.lblProductName.text = suggestedItem.name!
            
        }
        return cell
    }
    
}

//MARK: - Custom methods -

extension detailViewController {
    
    func createSilder(cell:sliderImageTableViewCell) -> sliderImageTableViewCell {
        
        cell.sliderImage.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.imageSliderDetafultHeight)
        
        let scrollViewWidth:CGFloat = cell.sliderImage.frame.width
        let scrollViewHeight:CGFloat = self.imageSliderDetafultHeight
        
        print("\(scrollViewWidth)")
        
        for index in 0...(self.selectedShopDetail.imagesList!.count - 1) {
            
            let imgOne = UIImageView(frame: CGRect(x:(CGFloat(index) * scrollViewWidth), y:0,width:scrollViewWidth, height:scrollViewHeight))
            
            imgOne.contentMode = .redraw
            
            if let imageName = self.selectedShopDetail.imagesList![index].imageUrl {
            
                imgOne.sd_setImage(with: URL(string: imageName), placeholderImage: UIImage(named: "img1"))
                 cell.sliderImage.addSubview(imgOne)
            
            }
            
        }
        
        cell.paginationSlider.numberOfPages = self.selectedShopDetail.imagesList!.count
        cell.sliderImage.contentSize = CGSize(width:cell.sliderImage.frame.width * CGFloat((self.selectedShopDetail.imagesList?.count)!), height:self.imageSliderDetafultHeight)
//        self.scrollView.delegate = self
//        self.paginationSlider.currentPage = 0
        
        return cell
    }

}

