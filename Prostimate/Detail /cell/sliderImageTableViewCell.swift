//
//  sliderImageTableViewCell.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class sliderImageTableViewCell: UITableViewCell, UIScrollViewDelegate {

    @IBOutlet var paginationSlider: UIPageControl!
    @IBOutlet var sliderImage: UIScrollView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.sliderImage.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //Scrollview delegate methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Test the offset and calculate the current page after scrolling ends
       
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        self.paginationSlider.currentPage = Int(currentPage);
        
        print("Index = \(Int(currentPage))")
        
       
    }

}
