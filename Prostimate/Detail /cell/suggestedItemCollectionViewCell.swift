//
//  suggestedItemCollectionViewCell.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

class suggestedItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var imgProduct: UIImageView!
}
