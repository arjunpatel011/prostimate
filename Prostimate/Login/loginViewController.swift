//
//  loginViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import  Alamofire
import NVActivityIndicatorView


class loginViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet var txtPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet var txtEmail: SkyFloatingLabelTextFieldWithIcon!
    private let services = authenticationService()
  
    let activityLoader = ActivityData()
    let messagesObj = messagessPopup()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtEmail.iconType = .image
        self.txtPassword.iconType = .image
        
    
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}

//MARK: - Button Action methods -
extension loginViewController {
    
    @IBAction func btnForgotPasswordSelected(_ sender: Any) {
        
        let forgotPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "forgotPasswordViewController") as! forgotPasswordViewController
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
        
    }
    @IBAction func btnLoginSelected(_ sender: Any) {

 
        if self.isValidate() {
        
            var loginParameter = loginParameterModel.init(json: [:])
            loginParameter?.email = self.txtEmail.text
            loginParameter?.password = self.txtPassword.text
            loginParameter?.user_type = "N"
            loginParameter?.device_type = "A"
            loginParameter?.device_token = "adfhagf"
            loginParameter?.app_version = "1.1"
            loginParameter?.device_name = "iPhone"
            
            
            self.isLogin(user: loginParameter!)
        }
    
    }
    
}
//MARK: - Webservices methods -

extension loginViewController {
    
    func isLogin(user:loginParameterModel){
        
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityLoader)
        
        self.services.isAuthenticateUser(userParameter: user) { (result) in
            
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
          
            switch (result){
                
            case let .Success(user):
            
                print(user)
               
                let defaults = UserDefaults.standard
                defaults.set(user.toJSON(), forKey: "loginUserDetails")
                
                let mainTabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "mainTabbar")
                self.navigationController?.pushViewController(mainTabbarVC!, animated: true)
            
            case .Offline:
            
                print("Offline")
                self.messagesObj.errorMessages(message: internetConnectivity)
                
            case .NotFound:
                
                self.messagesObj.warrningMessages(message: "Invalid user.")
                print("Notfound")
            
            default:
            
                self.messagesObj.warrningMessages(message: "Invalid user.")
                print("defalit")
                break
            }

        }
    
    }
}

//MARK: - Textfield delegate methods -

extension loginViewController:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text {
            
            if textField == self.txtPassword {
            
                if text.count < 0 {
                    self.txtPassword.errorMessage = "Password"
                }
                else {
                    self.txtPassword.errorMessage = ""
                }
            }
            
            if  textField == self.txtEmail{
                
                if !(text.isValidEmail()) {
                    self.txtEmail.errorMessage = "Invalid email"
                    
                } else {
                    
                    self.txtEmail.errorMessage = ""

                }
               
            }
        }

    }
}

//MARK: - Custom methods -

extension loginViewController {
    
    func isValidate() -> Bool {
       
        if !(self.txtEmail.text!.isValidEmail()) {
        
            self.txtEmail.errorMessage = "Invalid email"
            return false
            
        }
        
        if (self.txtEmail.text!.isEmptyCharacter()) {
            
            self.txtEmail.errorMessage = "Invalid email"
            return false
        }
        
       
        
        if (self.txtPassword.text!.isEmptyCharacter()) {
        
            self.txtPassword.errorMessage = "Password"
            return false
        }
        
        return true
    }
}

