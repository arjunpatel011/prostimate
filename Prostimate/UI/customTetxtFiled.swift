//
//  customTetxtFiled.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

@IBDesignable class customTetxtFiled: UITextField {

    @IBInspectable var cornerRadius:CGFloat = 0
    
    @IBInspectable var borderWidth:CGFloat = 0
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        self.layer.cornerRadius = self.cornerRadius
        self.layer.masksToBounds = true
        self.layer.borderWidth = self.borderWidth
    }
 

}
