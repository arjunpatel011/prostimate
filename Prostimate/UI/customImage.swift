//
//  customImage.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

@IBDesignable class customImage: UIImageView {

    
    @IBInspectable var isRound:Bool = false {
        
        didSet{
            if isRound {
                
                self.layer.cornerRadius = self.frame.width / 2
                self.layer.masksToBounds = true
            }
        }
    }
    
    @IBInspectable var cornerRadius : CGFloat = 0 {
        didSet{
            
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        // Drawing code
    }
 

}
