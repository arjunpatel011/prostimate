//
//  customeButton.swift
//  Prostimate
//
//  Created by Arjun Patel on 06/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit

@IBDesignable class customeButton: UIButton {

   @IBInspectable var cornerRadius:CGFloat = 0 {
    
        didSet{
        
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    
    }

}
