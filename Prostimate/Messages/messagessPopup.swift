//
//  messagessPopup.swift
//  Prostimate
//
//  Created by Arjun Patel on 14/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import SwiftMessages

class messagessPopup {
    
    func warrningMessages(message:String){
        
        let warning = MessageView.viewFromNib(layout: .cardView)
        warning.configureTheme(.warning)
        warning.configureDropShadow()
        warning.safeAreaTopOffset = 30
        
        warning.configureContent(title: "Warning", body: message, iconText: "🤔")
        warning.button?.isHidden = true
        var warningConfig = SwiftMessages.defaultConfig
        warningConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        warningConfig.duration = .seconds(seconds: 1)
        SwiftMessages.show(config: warningConfig, view: warning)
    }
    
    func errorMessages(message:String){
        
        let warning = MessageView.viewFromNib(layout: .cardView)
        warning.configureTheme(.error)
        warning.configureDropShadow()
        warning.safeAreaTopOffset = 30
        
        warning.configureContent(title: "Oops!", body: message, iconText: "")
        warning.button?.isHidden = true
        var warningConfig = SwiftMessages.defaultConfig
        warningConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        warningConfig.duration = .seconds(seconds: 1.5)
        SwiftMessages.show(config: warningConfig, view: warning)
    }
}
