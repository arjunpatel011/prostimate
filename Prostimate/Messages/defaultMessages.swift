//
//  defaultMessages.swift
//  Prostimate
//
//  Created by Arjun Patel on 14/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import UIKit

let internetConnectivity = "The internet connection seems to be down. Please check it!"
