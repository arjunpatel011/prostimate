//
//  extenAppdelegate.swift
//  
//
//  Created by Arjun Patel on 15/02/18.
//

import Foundation
import UIKit
extension AppDelegate {
    
    func setRootView(){
        
        let navigationController: UINavigationController? = (self.window?.rootViewController as? UINavigationController)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        
        if (UserDefaults.standard.object(forKey: "loginUserDetails") != nil) {
        
            loginUserDetial.sharedInstance.user = UserDefaults.standard.object(forKey: "loginUserDetails") as! userDetail
            
            navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "mainTabbar"), animated: false)
            
        } else {
            
         navigationController?.pushViewController(storyboard.instantiateViewController(withIdentifier: "homeViewController"), animated: false)
    

        }
        
        
    }
}
