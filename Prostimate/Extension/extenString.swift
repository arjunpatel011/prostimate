//
//  File.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
   public func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self)
    }
    
  public func isEmptyCharacter() -> Bool {
        
        let temp = self.replacingOccurrences(of: " ", with: "")
        
        if temp.count == 0 {
            
            return true
        } else {
            
            return false
        }
       
    }
    
}
