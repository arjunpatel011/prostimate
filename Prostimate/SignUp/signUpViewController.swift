//
//  signUpViewController.swift
//  Prostimate
//
//  Created by Arjun Patel on 09/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import UIKit
import AVFoundation
import SkyFloatingLabelTextField
import  Alamofire
import NVActivityIndicatorView

class signUpViewController: UIViewController {

    @IBOutlet var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet var txtLastName: SkyFloatingLabelTextField!
    @IBOutlet var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet var btnCreateAccountSelected: customeButton!
    
    let activityLoader = ActivityData()
    let messagesObj = messagessPopup()
    private let services = authenticationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


    
}

//MARK: - Textfield delegate methods -

extension signUpViewController:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text {
            
            if textField == self.txtFirstName {
                
                if text.count < 0 {
                    self.txtFirstName.errorMessage = "FirstName"
                }
                else {
                    self.txtFirstName.errorMessage = ""
                }
            }
            
            if textField == self.txtLastName {
                
                if text.count < 0 {
                    self.txtLastName.errorMessage = "LastName"
                }
                else {
                    self.txtLastName.errorMessage = ""
                }
            }
            
            if textField == self.txtPassword {
                
                if text.count < 0 {
                    self.txtPassword.errorMessage = "Password"
                }
                else {
                    self.txtPassword.errorMessage = ""
                }
            }
            
            if  textField == self.txtEmail{
                
                if !(text.isValidEmail()) {
                    self.txtEmail.errorMessage = "Invalid email"
                    
                } else {
                    
                    self.txtEmail.errorMessage = ""
                    
                }
                
            }
        }
        
    }
}

//MARK: - Custom methods -

extension signUpViewController {
    
    func isValidate() -> Bool {
        
        if !(self.txtEmail.text!.isValidEmail()) {
            
            self.txtEmail.errorMessage = "Invalid email"
            return false
            
        }
        
        if (self.txtEmail.text!.isEmptyCharacter()) {
            
            self.txtEmail.errorMessage = "Invalid email"
            return false
        }
        
        if (self.txtFirstName.text!.isEmptyCharacter()) {
            
            self.txtFirstName.errorMessage = "FirstName"
            return false
        }
        if (self.txtLastName.text!.isEmptyCharacter()) {
            
            self.txtLastName.errorMessage = "LastName"
            return false
        }
        
        if (self.txtPassword.text!.isEmptyCharacter()) {
            
            self.txtPassword.errorMessage = "Password"
            return false
        }
        
        return true
    }
}

//MARK: - Button Action methods -

extension signUpViewController {
    
    @IBAction func btnCreateUserSelected(_ sender: Any) {
        
        if self.isValidate() {
            
            
            var signupParameter = signUpUserParameter.init(json: [:])
            signupParameter?.email = self.txtEmail.text
            signupParameter?.password = self.txtPassword.text
            signupParameter?.firstName = self.txtFirstName.text
            signupParameter?.lastName = self.txtLastName.text
            signupParameter?.user_type = "N"
            signupParameter?.device_type = "I"
            signupParameter?.device_token = "adfhagf"
            signupParameter?.app_version = "1.1"
            signupParameter?.deviceName = "iPhone"
            
            
            self.newRegigter(user: signupParameter!)
        }
        
    }
}

//MARK: - Webservices functions -

extension signUpViewController {
    
    func newRegigter(user:signUpUserParameter){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityLoader)
        
        self.services.registerNewUser(userParameter: user) { (result) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            switch (result){
                
            case let .Success(user):
                
                print(user)
                let mainTabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "mainTabbar")
                self.navigationController?.pushViewController(mainTabbarVC!, animated: true)
                
            case .Offline:
                
                print("Offline")
                self.messagesObj.errorMessages(message: internetConnectivity)
                
            case .NotFound:
                
                self.messagesObj.warrningMessages(message: "Email already exists.")
                print("Notfound")
                
            default:
                
                self.messagesObj.warrningMessages(message: "Email already exists.")
                print("defalit")
                break
            }
            
        }
        
    }

}
