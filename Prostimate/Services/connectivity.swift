//
//  connectivity.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Alamofire

class connectivity{
    
    class func isConnectedToInternet() ->Bool {
        
        return NetworkReachabilityManager()!.isReachable
    }
}
