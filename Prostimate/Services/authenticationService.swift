//
//  authenticationService.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import  Alamofire

/**
 This class is used to basic authentication services.
 */
class authenticationService {
    
    
    let isConnected = connectivity.isConnectedToInternet()
    
    struct basicAuthenticationUrl {
        
      static let login = "beforeauth/login"
      static let newRegistration = "beforeauth/signup"
    }
    
    enum isValidUser {

        case NotFound
        case Success(userDetail)
        case Failure(NSError)
        case Offline
    }

    /**
     This services is used to register new user.
 */
    
    func registerNewUser(userParameter:signUpUserParameter ,completion: @escaping (isValidUser)->()) {
        
        print(self.isConnected)
        
        if self.isConnected {
            
            let parameter : [String:AnyObject] = userParameter.toJSON()! as [String : AnyObject]
            
            print(parameter)
            
            let url = "\(baseUrl)\(basicAuthenticationUrl.newRegistration)"
            print(url)
            
            Alamofire.request(
                URL(string: url)!,
                method: .post,
                parameters: parameter)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    print("response = \(response)")
                    
                    switch response.result {
                        
                    case .success(_):
                        
                        let dataDict = response.result.value as! [String:AnyObject]
                        print("Data Dict = \(dataDict)")
                        
                        if (dataDict["status"] as! Bool) {
                            
                            let user = userDetail(json: dataDict["data"] as! [String:AnyObject])
                            completion(.Success(user!))
                            
                        } else {
                            
                            completion(.NotFound)
                            
                        }
                        
                        
                    case .failure:
                        
                        print("Failre")
                        
                        completion(.Failure(response.result.error! as NSError))
                        
                    }
            }
        } else {
            
            completion(.Offline)
        }
    }
    
    
    /**
     This services is used to check user valid authentication.
     */
    func isAuthenticateUser(userParameter:loginParameterModel, completion: @escaping (isValidUser)->()) {
        
        print(self.isConnected)
        
        if self.isConnected {
            
            let parameter : [String:AnyObject] = userParameter.toJSON()! as [String : AnyObject]
            
            let url = "\(baseUrl)\(basicAuthenticationUrl.login)"
            print(url)
            
            Alamofire.request(
                URL(string: url)!,
                method: .post,
                parameters: parameter)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    print("response = \(response)")
                    
                    switch response.result {
                        
                    case .success(_):
                        
                        let dataDict = response.result.value as! [String:AnyObject]
                        print("Data Dict = \(dataDict)")
                        
                        if (dataDict["status"] as! Bool) {
                        
                            let user = userDetail(json: dataDict["data"] as! [String:AnyObject])
                            completion(.Success(user!))

                      } else {
                        
                            completion(.NotFound)
                       
                        }
                        
                        
                    case .failure:
                        
                        print("Failre")
                        
                        completion(.Failure(response.result.error! as NSError))
                        
                    }
            }
        } else {
            
            completion(.Offline)
        }
    }
 }
    
    

