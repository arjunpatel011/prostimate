//
//  shopingServices.swift
//  Prostimate
//
//  Created by Arjun Patel on 14/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import  Alamofire
import Gloss
/**
 This class is used to basic authentication services.
 */
class shopingServices {
    
    
    let isConnected = connectivity.isConnectedToInternet()
    let loginUser = loginUserDetial.sharedInstance.user
    struct basicAuthenticationUrl {
        
        static let retriveShopingList = "beforeauth/getshoplist"
        static let retriveShopDetail = "beforeauth/getshopdetail"
        static let retriveShopingHistory = "auth/getredeemhistory"
        
    }
    
    enum shopingList {
        
        case NotFound
        case Success([shopList])
        case Failure(NSError)
        case Offline
    }
    
    enum shopDescription {
        
        case NotFound
        case Success(shopDetail)
        case Failure(NSError)
        case Offline
    }
    
    enum shopHistroty {
        
        case NotFound
        case Success([shopingHistory])
        case Failure(NSError)
        case Offline
    }
    
    /**
     This services is used to retrive shoping list.
     */
    
    func retriveShopingList(latitude:String ,longitude:String,completion: @escaping (shopingList)->()) {
        
        print(self.isConnected)
        
        if self.isConnected {
            
            var parameter : [String:AnyObject] = [:]
            parameter["latitude"] = latitude as AnyObject
            parameter["longitude"] = longitude as AnyObject
            print(parameter)
            
            let url = "\(baseUrl)\(basicAuthenticationUrl.retriveShopingList)"
            print(url)
            
            Alamofire.request(
                URL(string: url)!,
                method: .post,
                parameters: parameter)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    print("response = \(response)")
                    
                    switch response.result {
                        
                    case .success(_):
                        
                        let dataDict = response.result.value as! [String:AnyObject]
                        print("Data Dict = \(dataDict)")
                        
                        if (dataDict["status"] as! Bool) {
                            
                            let shopListObj = [shopList].from(jsonArray: dataDict["data"] as! [JSON] )
                            completion(.Success(shopListObj!))
                            
                        } else {
                            
                            completion(.NotFound)
                            
                        }
                        
                    case .failure:
                        
                        print("Failre")
                        
                        completion(.Failure(response.result.error! as NSError))
                        
                    }
            }
        } else {
            
            completion(.Offline)
        }
    }
    
    /**
     This services is used to retrive shop detail.
     */
    
    func retriveShopDetail(shopId:String ,completion: @escaping (shopDescription)->()) {
        
        print(self.isConnected)
        
        if self.isConnected {
            
            var parameter : [String:AnyObject] = [:]
            parameter["shop_id"] = shopId as AnyObject
            
            print(parameter)
            
            let url = "\(baseUrl)\(basicAuthenticationUrl.retriveShopDetail)"
            print(url)
            
            Alamofire.request(
                URL(string: url)!,
                method: .post,
                parameters: parameter)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    print("response = \(response)")
                    
                    switch response.result {
                        
                    case .success(_):
                        
                        let dataDict = response.result.value as! [String:AnyObject]
                        print("Data Dict = \(dataDict)")
                        
                        if (dataDict["status"] as! Bool) {
                            
                            //let shopObj = shopDetail.init(json: dataDict["data"] as! [String:AnyObject])
                            
                            var shopObjList = [shopDetail].from(jsonArray:dataDict["data"]  as! [JSON])
                            completion(.Success(shopObjList![0]))
                            
                            
                        } else {
                            
                            completion(.NotFound)
                            
                        }
                        
                    case .failure:
                        
                        print("Failre")
                        
                        completion(.Failure(response.result.error! as NSError))
                        
                    }
            }
        } else {
            
            completion(.Offline)
        }
    }

    /**
     This services is used to retrive user history.
     */
    
    func retriveShopHistory(completion: @escaping (shopHistroty)->()) {
        
        print(self.isConnected)
        
        if self.isConnected {
            
            var parameter : [String:AnyObject] = [:]
            parameter["auth_key"] = self.loginUser?.authKey as AnyObject
            
            print(parameter)
            
            let url = "\(baseUrl)\(basicAuthenticationUrl.retriveShopingHistory)"
            print(url)
            
            Alamofire.request(
                URL(string: url)!,
                method: .post,
                parameters: parameter)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    print("response = \(response)")
                    
                    switch response.result {
                        
                    case .success(_):
                        
                        let dataDict = response.result.value as! [String:AnyObject]
                        print("Data Dict = \(dataDict)")
                        
                        if (dataDict["status"] as! Bool) {
                            
                            var shopObjList = [shopingHistory].from(jsonArray:dataDict["data"]  as! [JSON])
                          
                            completion(.Success(shopObjList))!
                            
                            
                        } else {
                            
                            completion(.NotFound)
                            
                        }
                        
                    case .failure:
                        
                        print("Failre")
                        
                        completion(.Failure(response.result.error! as NSError))
                        
                    }
            }
        } else {
            
            completion(.Offline)
        }
    }

    
   }



