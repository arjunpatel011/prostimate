//
//  suggestedItems.swift
//  Prostimate
//
//  Created by Arjun Patel on 15/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct suggestedItems: Glossy {
    
    var menuId:Int?
    var shopId:Int?
    var name:String?
    var imageUrl:String?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.menuId = "menu_id" <~~ json
        self.shopId = "shop_id" <~~ json
        self.name = "name" <~~ json
        self.imageUrl = "image_url" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "image_url" ~~> self.imageUrl,
            "shop_id" ~~> self.shopId,
            "name" ~~> self.name,
            "menu_id" ~~> self.menuId
            
            ])
    }
    
}

