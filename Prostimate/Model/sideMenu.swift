//
//  sideMenu.swift
//  Prostimate
//
//  Created by Arjun Patel on 10/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation

struct sideMenu {
    
    var icon:String!
    var title:String!
    
    init(icon:String,title:String) {
        
        self.icon = icon
        self.title = title
    }
}
