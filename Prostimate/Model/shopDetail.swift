//
//  shopDetail.swift
//  Prostimate
//
//  Created by Arjun Patel on 15/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct shopDetail: Glossy {
    
    var shopId:String?
    var name:String?
    var neighbourhood:String?
    var latitude:String?
    var logitude:String?
    var mobile:Int?
    var address:String?
    var description:String?
    var monday:String?
    var mondayTime:String?
    var tuesday:String?
    var tuesdayTime:String?
    var wednsday:String?
    var wednsdayTime:String?
    var thrusday:String?
    var thrusdayTime:String?
    var friday:String?
    var fridayTime:String?
    var saturday:String?
    var saturdayTime:String?
    var sunday:String?
    var sundayTime:String?
    var imagesList:[listImages]?
    var suggestedItmsList:[suggestedItems]?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.shopId = "shop_id" <~~ json
        self.name = "name" <~~ json
        self.neighbourhood = "neighbourhood" <~~ json
        self.latitude = "latitude" <~~ json
        self.logitude = "longitude" <~~ json
        self.mobile = "mobile" <~~ json
        self.address = "address" <~~ json
        self.description = "description" <~~ json
        self.monday = "monday" <~~ json
        self.tuesday = "tuesday" <~~ json
        self.wednsday = "wednsday" <~~ json
        self.thrusday = "thursday" <~~ json
        self.friday = "friday" <~~ json
        self.saturday = "saturday" <~~ json
        self.sunday = "sunday" <~~ json
        self.mondayTime = "monday_time" <~~ json
        self.tuesdayTime = "tuesday_time" <~~ json
        self.wednsdayTime = "wednsday_time" <~~ json
        self.thrusdayTime = "thursday_time" <~~ json
        self.fridayTime = "friday_time" <~~ json
        self.saturdayTime = "saturday_time" <~~ json
        self.sundayTime = "sunday_time" <~~ json
        self.imagesList = "allimage" <~~ json
        self.suggestedItmsList = "allmenu" <~~ json
        
        if let images = json["allimage"] as? [[String : AnyObject]]
        {
            self.imagesList = [listImages].from(jsonArray: images)
        }
        else
        {
            self.imagesList = []
        }
        
        if let suggest = json["allmenu"] as? [[String : AnyObject]]
        {
            self.suggestedItmsList = [suggestedItems].from(jsonArray: suggest)
        }
        else
        {
            self.suggestedItmsList = []
        }
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "shop_id" ~~> self.shopId,
            "name" ~~> self.name,
            "neighbourhood" ~~> self.neighbourhood,
            "latitude" ~~> self.latitude,
            "logitude" ~~> self.logitude,
            
            ])
    }
    
}
