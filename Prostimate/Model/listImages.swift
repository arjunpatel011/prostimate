//
//  listImages.swift
//  Prostimate
//
//  Created by Arjun Patel on 15/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct listImages: Glossy {
    
    var imageUrl:String?
   
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.imageUrl = "image_url" <~~ json
            
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "image_url" ~~> self.imageUrl,
            
            ])
    }
    
}

