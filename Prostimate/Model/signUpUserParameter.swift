//
//  signUpUserParameter.swift
//  Prostimate
//
//  Created by Arjun Patel on 14/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct signUpUserParameter: Glossy {
    
    
    var firstName: String?
    var lastName: String?
    var email: String?
    var password:String?
    var user_type: String?
    var device_token: String?
    var device_type:String?
    var app_version: String?
    var deviceName:String?
    
    init?(json: JSON) {
        
        self.password = "password" <~~ json
        self.firstName = "first_name" <~~ json
        self.lastName = "last_name" <~~ json
        self.email = "email" <~~ json
        self.user_type = "user_type" <~~ json
        self.device_type = "device_type" <~~ json
        self.device_token = "device_token" <~~ json
        self.app_version = "app_version" <~~ json
        self.deviceName = "device_name" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            
            "User[first_name]" ~~> self.firstName,
            "User[last_name]" ~~> self.lastName,
            "User[email]" ~~> self.email,
            "User[password]" ~~> self.password,
            "User[user_type]" ~~> self.user_type,
            "User[device_type]" ~~> self.device_type,
            "User[device_token]" ~~> self.device_token,
            "User[app_version]" ~~> self.app_version,
            
            
            
            ])
    }
    
}
