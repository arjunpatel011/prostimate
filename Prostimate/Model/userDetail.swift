//
//  userDetail.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation

import Gloss

struct userDetail: Glossy {
   
    var userId: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var user_type: String?
    var referralCode: String?
    var profileUrl : String?
    var contactNo:String?
    var device_token: String?
    var device_type:String?
    var app_version: String?
    var isActive:String?
    var authKey:String?
    
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        
        self.userId = "user_id" <~~ json
        self.firstName = "first_name" <~~ json
        self.lastName = "last_name" <~~ json
        self.email = "email" <~~ json
        self.user_type = "user_type" <~~ json
        self.referralCode = "referral_code" <~~ json
        self.profileUrl = "image_url" <~~ json
        self.contactNo = "contact_no" <~~ json
        self.device_type = "device_type" <~~ json
        self.device_token = "device_token" <~~ json
        self.app_version = "app_version" <~~ json
        self.isActive = "is_active" <~~ json
        self.authKey = "auth_key" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "user_id" ~~> self.userId,
            "first_name" ~~> self.firstName,
            "last_name" ~~> self.lastName,
            "email" ~~> self.email,
            "user_type" ~~> self.user_type,
            "referral_code" ~~> self.referralCode,
            "image_url" ~~> self.profileUrl,
            "contact_no" ~~> self.contactNo,
            "device_type" ~~> self.device_type,
            "device_token" ~~> self.device_token,
            "app_version" ~~> self.app_version,
            "is_active" ~~> self.isActive,
            "auth_key" ~~> self.authKey
            
            ])
    }
    
}
