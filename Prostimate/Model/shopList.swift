//
//  shopList.swift
//  Prostimate
//
//  Created by Arjun Patel on 14/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct shopList: Glossy {
    
    var shopId:String?
    var name:String?
    var neighbourhood:String?
    var latitude:String?
    var logitude:String?
    var distance:String?
    var time:String?
    var imageProfile:String?
    
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.shopId = "shop_id" <~~ json
        self.name = "name" <~~ json
        self.neighbourhood = "neighbourhood" <~~ json
        self.latitude = "latitude" <~~ json
        self.logitude = "longitude" <~~ json
        self.distance = "distance" <~~ json
        self.time = "time" <~~ json
        self.imageProfile = "image_url" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "shop_id" ~~> self.shopId,
            "name" ~~> self.name,
            "neighbourhood" ~~> self.neighbourhood,
            "latitude" ~~> self.latitude,
            "logitude" ~~> self.logitude,
            "distance" ~~> self.distance,
            "time" ~~> self.time,
            "image_url" ~~> self.imageProfile
            ])
    }
    
}
