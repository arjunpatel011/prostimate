//
//  shopingHistory.swift
//  Prostimate
//
//  Created by Arjun Patel on 15/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct shopingHistory: Glossy {
    
    var noOfDrink:Int?
    var userID:String?
    var shopId:Int?
    var purchasePlanId:Int?
    var month:String?
    var shopName:String?
    var shopImage:String?
    var drink:[historyDrink]?
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.noOfDrink = "no_of_drink" <~~ json
        self.userID = "user_id" <~~ json
        self.shopId = "shop_id" <~~ json
        self.purchasePlanId = "purchase_plan_id" <~~ json
        self.month = "month" <~~ json
        self.shopName = "shop_name" <~~ json
        self.shopImage = "shop_image" <~~ json
        
        if let drinks = json["drink"] as? [[String : AnyObject]]
        {
            self.drink = [historyDrink].from(jsonArray: drinks)
        }
        else
        {
            self.drink = []
        }
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "no_of_drink" ~~> self.noOfDrink,
            "user_id" ~~> self.userID,
            "shop_id" ~~> self.shopId,
            "purchase_plan_id" ~~> self.purchasePlanId,
            "month" ~~> self.month,
            "drink" ~~> self.drink?.toJSONArray(),
            "shop_name" ~~> self.shopName
            
            ])
    }
    
}

