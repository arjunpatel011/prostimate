//
//  historyDrink.swift
//  Prostimate
//
//  Created by Arjun Patel on 15/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct historyDrink: Glossy {
    
    var redeemId:Int?
    var menuId:Int?
    var userCode:String?
    var created:String?
    var updated:String?
    var drinkName:String?
    var drinkImage:String?
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.redeemId = "redeem_id" <~~ json
        self.menuId = "menu_id" <~~ json
        self.userCode = "usercode" <~~ json
        self.created = "created_at" <~~ json
        self.updated = "updated_at" <~~ json
        self.drinkName = "drink_name" <~~ json
        self.drinkImage = "drink_image" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "redeem_id" ~~> self.redeemId,
            "menu_id" ~~> self.menuId,
            "usercode" ~~> self.userCode,
            "created_at" ~~> self.created,
            "updated_at" ~~> self.updated,
            "drink_name" ~~> self.drinkName,
            "drink_image" ~~> self.drinkImage
            
            ])
    }
    
}

