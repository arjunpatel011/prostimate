//
//  loginModel.swift
//  Prostimate
//
//  Created by Arjun Patel on 12/02/18.
//  Copyright © 2018 WebFlux Solutions. All rights reserved.
//

import Foundation
import Gloss

struct loginParameterModel: Glossy {
    
    var email: String!
    var password: String!
    var user_type: String!
    var device_type: String!
    var device_token: String!
    var app_version: String!
    var device_name: String!
    
    // MARK: - Deserialization
    
    init?(json: JSON) {
        self.email = "email" <~~ json
        self.password = "password" <~~ json
        self.user_type = "user_type" <~~ json
        self.device_type = "device_type" <~~ json
        self.device_token = "device_token" <~~ json
        self.app_version = "app_version" <~~ json
        self.device_name = "device_name" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "email" ~~> self.email,
            "password" ~~> self.password,
            "user_type" ~~> self.user_type,
            "device_type" ~~> self.device_type,
            "device_token" ~~> self.device_token,
            "app_version" ~~> self.app_version,
            "device_name" ~~> self.device_name
            ])
    }
    
}
